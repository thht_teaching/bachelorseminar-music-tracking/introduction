## Recording {.smaller}
* Our lectures will be recorded.
* Your presentations will not be recorded.
* If you join via MS Teams:
    * If you say something, and your camera is on, the video of your camera will be recorded!
* We will share the recordings via MS Teams.
* You must not publish, share or upload these recordings.
* If someone who is not part of this seminar wants the recordings, please refer that person to us!